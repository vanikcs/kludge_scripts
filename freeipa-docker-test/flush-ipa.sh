#!/bin/bash
sudo docker-compose --verbose down &> /dev/null
sudo docker volume prune -f &> /dev/null
sudo rm -rf /var/lib/ipa-data/*
sudo rm -rf /var/lib/ipa-data/.* &> /dev/null
