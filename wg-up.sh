#!/bin/bash
#Set your gateway ip
YOUR_SITE_IP=1.1.1.2
#Set remote gateway ip
SEC_SITE_IP=2.2.2.2
#Set your local subnets
LOCAL_NET=10.10.0.0/16
#Set remote subnets
REMOTE_NET=10.20.0.0/16

for pack in sshpass wireguard
do
if [ -n "$(dpkg -l | grep $pack)" ]
then
echo "$pack is installed"
else
apt install $pack -y
fi
done
cd /etc/wireguard/

wg genkey | tee server.key | wg pubkey > server.pub
sleep 1
wg genkey | tee client.key | wg pubkey > client.pub
sleep 1


SERVER_PRIV=`cat server.key`
CLIENT_PUB=`cat client.pub`
CLIENT_PRIV=`cat client.key`
SERVER_PUB=`cat server.pub`


echo "[Interface]" > wg0.conf
echo "PrivateKey = $SERVER_PRIV" >> wg0.conf
echo "Address = 10.100.100.1/30" >> wg0.conf
echo "ListenPort = 1800" >> wg0.conf
echo "" >> wg0.conf
echo "[Peer]" >> wg0.conf
echo "PublicKey =  $CLIENT_PUB" >> wg0.conf

echo "AllowedIPs = $REMOTE_NET,10.100.100.0/30" >> wg0.conf
echo "Endpoint = $SEC_SITE_IP:1800" >> wg0.conf

echo "[Interface]" > wg0client
echo "PrivateKey = $CLIENT_PRIV" >> wg0client
echo "Address = 10.100.100.2/30" >> wg0client
echo "ListenPort = 1800" >> wg0client
echo "" >> wg0client
echo "[Peer]" >> wg0client
echo "PublicKey =  $SERVER_PUB" >> wg0client

echo "AllowedIPs = $LOCAL_NET,10.100.100.0/30" >> wg0client
echo "Endpoint = $YOUR_SITE_IP:1800" >> wg0client

scp client.key root@$SEC_SITE_IP:/etc/wireguard/client.key
scp wg0client root@$SEC_SITE_IP:/etc/wireguard/wg0.conf

rm -f wg0client

if [ "$(systemctl is-enabled wg-quick@wg0.service)" == "enabled" ]; then
echo "========================"
echo "Reconfiguring local peer"
echo "========================"
wg-quick down wg0
wg-quick up wg0
else
echo "========================"
echo "Enabling local peer"
echo "========================"
systemctl enable --now wg-quick@wg0
fi

if [ -n "$(sshpass ssh root@$SEC_SITE_IP "/bin/bash -c 'dpkg -l | grep wireguard'")" ]
then
echo "wireguard is installed"
else
sshpass ssh root@$SEC_SITE_IP "/bin/bash -c 'apt install wireguard -y'"
fi

if [ "$(sshpass ssh root@$SEC_SITE_IP "/bin/bash -c 'systemctl is-enabled wg-quick@wg0'")" == "enabled" ]; then
echo "========================="
echo "Reconfiguring remote peer"
echo "========================="
sshpass ssh root@$SEC_SITE_IP "/bin/bash -c 'wg-quick down wg0'"
sshpass ssh root@$SEC_SITE_IP "/bin/bash -c 'wg-quick up wg0'"
else
echo "========================="
echo "Enabling remote peer"
echo "========================="
sshpass ssh root@$SEC_SITE_IP "/bin/bash -c 'systemctl enable --now wg-quick@wg0'"
fi
